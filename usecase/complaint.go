package usecase

import "bitbucket.org/nigeon/pukeonit/domain"

type Complaint struct {
	ID    int
	Title string
}

type ComplaintInteractor struct {
	ComplaintRepository domain.ComplaintRepository
}

//Add exposes the logic for creating and storing a new complaint
func (interactor *ComplaintInteractor) Add(complaint domain.Complaint) error {
	//Do whatever the logic we need
	//...

	//Persist now
	interactor.ComplaintRepository.Add(complaint)

	return nil
}

func (interactor *ComplaintInteractor) List() (domain.Complaints, error) {
	complaints, err := interactor.ComplaintRepository.FindAll()
	return complaints, err
}

func (interactor *ComplaintInteractor) Get(ID int) (domain.Complaint, error) {
	complaint, err := interactor.ComplaintRepository.Find(ID)

	return complaint, err
}
