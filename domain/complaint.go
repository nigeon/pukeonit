package domain

type Complaint struct {
	ID      int
	Title   string
	Created string
}

//GetTitle return the complaint title for a given ComplaintID
func (complaint *Complaint) GetTitle() string {
	return complaint.Title
}
