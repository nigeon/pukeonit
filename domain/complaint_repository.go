package domain

type Complaints []Complaint

type ComplaintRepository interface {
	Find(complaintID int) (Complaint, error)
	FindAll() (Complaints, error)
	Add(Complaint)
	Update(Complaint)
	Delete(complaintID int)
}
