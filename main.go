package main

import (
	"net/http"

	"bitbucket.org/nigeon/pukeonit/infrastructure"
	"bitbucket.org/nigeon/pukeonit/interface/repository"
	"bitbucket.org/nigeon/pukeonit/interface/webservice"

	"github.com/gin-gonic/gin"
)

func main() {
	// Creates a gin router with default middleware:
	// logger and recovery (crash-free) middleware
	router := gin.Default()

	//DB setup
	dbHandler := infrastructure.NewSqliteHandler("/tmp/pukeonit.sqlite")
	handlers := make(map[string]repository.DbHandler)
	handlers["DbComplaintRepository"] = dbHandler

	v1 := router.Group("/v1")
	{
		complaint := new(webservice.ComplaintHandler)
		complaint.ComplaintInteractor.ComplaintRepository = repository.NewDbComplaintRepository(handlers)

		v1.GET("/complaints", complaint.Index)
		v1.POST("/complaints", complaint.Create)
		v1.GET("/complaints/:id", complaint.Get)
		v1.PUT("/complaints/:id", complaint.Update)
		v1.DELETE("/complaints/:id", complaint.Delete)
	}

	router.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "hi there!",
		})
	})

	router.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "404 not found!",
		})
	})

	// By default it serves on :8080 unless a
	// PORT environment variable was defined.
	router.Run(":8888")
	// router.Run(":3000") for a hard coded port
}
