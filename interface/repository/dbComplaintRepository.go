package repository

import (
	"fmt"

	"bitbucket.org/nigeon/pukeonit/domain"
)

type DbComplaintRepository DbRepo

//Find method
func (repo *DbComplaintRepository) Find(ID int) (domain.Complaint, error) {
	row := repo.dbHandler.Query(fmt.Sprintf("SELECT * FROM complaints WHERE id = '%d' LIMIT 1", ID))
	var id int
	var title string
	row.Next()
	row.Scan(&id, &title)
	complaint := domain.Complaint{ID: id, Title: title}
	return complaint, nil
}

//FindAll method
func (repo *DbComplaintRepository) FindAll() (domain.Complaints, error) {
	var cs domain.Complaints
	rows := repo.dbHandler.Query("SELECT * FROM complaints")
	for rows.Next() {
		var id int
		var title string
		rows.Scan(&id, &title)
		cs = append(cs, domain.Complaint{ID: id, Title: title})
	}

	return cs, nil
}

func (repo *DbComplaintRepository) Add(complaint domain.Complaint) {
	repo.dbHandler.Execute(fmt.Sprintf("INSERT INTO complaints (title) VALUES ('%v')", complaint.Title))
}

func (repo *DbComplaintRepository) Update(complaint domain.Complaint) {
	repo.dbHandler.Execute(fmt.Sprintf("UPDATE complaints (title) SET title = '%v' WHERE id = '%d'", complaint.Title, complaint.ID))
}

func (repo *DbComplaintRepository) Delete(ID int) {
	repo.dbHandler.Execute(fmt.Sprintf("DELETE FROM complaints WHERE id = '%d'", ID))
}

func NewDbComplaintRepository(dbHandlers map[string]DbHandler) *DbComplaintRepository {
	dbComplaintRepository := new(DbComplaintRepository)
	dbComplaintRepository.dbHandlers = dbHandlers
	dbComplaintRepository.dbHandler = dbHandlers["DbComplaintRepository"]
	return dbComplaintRepository
}
