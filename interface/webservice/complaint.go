package webservice

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/nigeon/pukeonit/domain"
	"bitbucket.org/nigeon/pukeonit/usecase"

	"github.com/gin-gonic/gin"
)

//ComplaintHandler definition
type ComplaintHandler struct {
	ComplaintInteractor usecase.ComplaintInteractor
}

//Index lists collection og complaints
func (handler ComplaintHandler) Index(c *gin.Context) {
	complaintList, err := handler.ComplaintInteractor.List()
	if err != nil {
		fmt.Println(err)
	}

	c.JSON(http.StatusOK, complaintList)
}

//Get the complaint
func (handler ComplaintHandler) Get(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		fmt.Println(err)
	}

	complaint, err := handler.ComplaintInteractor.Get(id)

	c.JSON(http.StatusOK, complaint)
}

//Create a complaint
func (handler ComplaintHandler) Create(c *gin.Context) {
	title := c.PostForm("title")

	t := time.Now()
	complaint := domain.Complaint{Title: title, Created: t.Format("2006-01-02 15:04:05")}
	handler.ComplaintInteractor.Add(complaint)

	c.JSON(http.StatusOK, complaint)
}

//Update the complaint
func (handler ComplaintHandler) Update(c *gin.Context) {
}

//Delete the complaint
func (handler ComplaintHandler) Delete(c *gin.Context) {

}
