CREATE TABLE `complaints` (
    `id` INTEGER PRIMARY KEY AUTOINCREMENT,
    `title` VARCHAR(64) NULL,
    `created` DATE NULL
);
